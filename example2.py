"""
	Example using aiohttp.
	
	TODO: More complex setup involving:
	[x] initial HTTP server that only responds to HTTP01 challenges
	[ ] on startup, tries to get the cert; if doesn't exist, it gets one
	[ ] once it has a cert, it starts an HTTPS server
	[ ] periodic task that renews certs near expiry
"""

from aiohttp import web
from acme import challenges

import simple_acme

def main():
	app = web.Application()
	app['challenge_responder'] = ExampleChallengeResponder()
	app.router.add_get('/.well-known/acme-challenge/{chall}', handle_http01_challenge)
	web.run_app(app, host = '0.0.0.0', port = 80)

async def handle_http01_challenge(request):
	challenge_responder = request.app['challenge_responder']
	validation = challenge_responder.get_validation(request.path)
	if validation is None:
		return web.Response(status = 404)
	return web.Response(status = 200, body = validation.encode(), content_type = 'text/plain')

class ExampleChallengeResponder(simple_acme.ChallengeResponder):
	CHALLENGE_TYPE = challenges.HTTP01
	
	def __init__(self) -> None:
		super().__init__()
		self.challs = {}
	
	def get_validation(self, path: str):
		return self.challs.get(path)
	
	def setup_challenge(self, chall, response, validation, client: Client) -> None:
		self.challs[chall.path] = validation
		if not response.simple_verify(chall, 'localhost', client.key.public_key(), self.port):
			raise Exception("`simple_verify` failed")
	
	def teardown_challenge(self, chall) -> None:
		self.challs.pop(chall.path, None)

if __name__ == '__main__':
	print("Nothing to see here, just look at the file.")
