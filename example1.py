"""
	Example `ChallengeResponder` using `acme.standalone.HTTP01DualNetworkedServers`.
"""

from acme import challenges
from acme.standalone import HTTP01RequestHandler, HTTP01DualNetworkedServers
from acme.client import Client

import simple_acme

class StandaloneHTTP01ChallengeResponder(simple_acme.ChallengeResponder):
	CHALLENGE_TYPE = challenges.HTTP01
	
	def __init__(self, *, port: int = None) -> None:
		super().__init__()
		self.port = port or 80
		self.servers_by_chall = {}
	
	def setup_challenge(self, chall, response, validation, client: Client) -> None:
		# Responds at the url given in `chall.path`
		
		address = ('0.0.0.0', self.port)
		resource = HTTP01RequestHandler.HTTP01Resource(chall = chall, response = response, validation = validation)
		servers = HTTP01DualNetworkedServers(address, { resource })
		self.servers_by_chall[chall] = servers
		servers.serve_forever()
		
		if not response.simple_verify(chall, 'localhost', client.key.public_key(), self.port):
			raise Exception("`simple_verify` failed")
	
	def teardown_challenge(self, chall) -> None:
		servers = self.servers_by_chall.pop(chall, None)
		if not servers: return
		servers.shutdown_and_server_close()

if __name__ == '__main__':
	print("Nothing to see here, just look at the file.")
