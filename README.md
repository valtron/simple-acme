# simple-acme

Tool to make using the ACME protocol easier.

```python
import simple_acme

# Uses LetsEncrypt directory by default
client = simple_acme.Client()

# Get/create account (if file doesn't already exist)
account = client.account('account.json', email = 'email@example.com')

# Get/renew cert
(pkey_fname, cert_fname, chain_fname) = account.get_cert('domain.tld', 'store-certs-here/', MyChallengeResponder())

class MyChallengeResponder(simple_acme.ChallengeResponder):
	# See `example1.py` and `example2.py`.
```
