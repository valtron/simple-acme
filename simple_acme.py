import json
from pathlib import Path
from contextlib import contextmanager

import josepy as jose
from acme import messages
from acme import client

LETSENCRYPT_DIRECTORY = 'https://acme-v01.api.letsencrypt.org/directory'
USER_AGENT = 'python/simple_acme'
ACC_KEY_SIZE = 2048
CERT_PKEY_BITS = 2048

class Client:
	def __init__(self, directory_url = None) -> None:
		self.directory_url = directory_url or LETSENCRYPT_DIRECTORY
	
	def account(self, fname, *, email = None) -> 'Account':
		p = Path(fname)
		if p.exists():
			with p.open() as fh:
				data = json.load(fh)
			acc_key = jose.JWK.from_json(data['account_key'])
			regr = messages.RegistrationResource.from_json(data['registration'])
		else:
			assert email
			
			from cryptography.hazmat.backends import default_backend
			from cryptography.hazmat.primitives.asymmetric import rsa
			acc_key = jose.JWKRSA(key = rsa.generate_private_key(public_exponent = 65537, key_size=ACC_KEY_SIZE, backend = default_backend()))
			
			client_acme = client.Client(self.directory_url, acc_key, net = client.ClientNetwork(acc_key, user_agent = USER_AGENT))
			regr = client_acme.agree_to_tos(client_acme.register(messages.NewRegistration.from_data(email = email)))
			
			with p.open('w') as fh:
				json.dump(fh, {
					'account_key': acc_key.to_json(),
					'registration': regr.to_json(),
				})
		
		return Account(self.directory_url, acc_key)

class Account:
	def __init__(self, directory_url, acc_key) -> None:
		self.client_acme = client.Client(directory_url, acc_key, net = client.ClientNetwork(acc_key, user_agent = USER_AGENT))
	
	def get_cert(self, domain, file_prefix, challenge_responder):
		"""
			#1 me -> LE
				req: "i want cert for <domain>"
				resp: "ok, pick one of <challenges>"
			#2 me -> LE
				req: "i choose <chall>"
				resp: "ok gimme a sec"
			#3 LE -> <chall.target>
				req: "gimme response for <chall>"
				resp: <chall.response>
			#4 me -> LE
				req: "gimme the cert now"
				resp: <cert>
		"""
		
		from OpenSSL import crypto
		from acme import crypto_util
		
		prefix = file_prefix + domain
		
		# Get or create pkey
		pkey_filepath = prefix + '-privkey.pem'
		if Path(pkey_filepath).exists():
			with open(pkey_filepath, 'rb') as fh:
				pkey = crypto.load_privatekey(crypto.FILETYPE_PEM, fh.read())
			pkey_pem = crypto.dump_privatekey(crypto.FILETYPE_PEM, pkey)
		else:
			pkey = crypto.PKey()
			pkey.generate_key(crypto.TYPE_RSA, CERT_PKEY_BITS)
			pkey_pem = crypto.dump_privatekey(crypto.FILETYPE_PEM, pkey)
			with open(pkey_filepath, 'wb') as fh:
				fh.write(pkey_pem)
		
		# Part #1: Get challenges
		authz = self.client_acme.request_domain_challenges(domain)
		if not contains_valid_chall(authz):
			challb = challenge_responder.select_challenge(authz)
			assert challb
			response, validation = challb.response_and_validation(self.client_acme.key)
			
			with challenge_responder_context(challenge_responder, challb.chall, response, validation, self.client_acme):
				# Part #2: Tell LetsEncrypt to validate selected challenge
				self.client_acme.answer_challenge(challb, response)
				# Part #3: Wait for LetsEncrypt to become convinced we fulfilled the challenge
				self.client_acme.poll(authz)
		
		# Part #4: Get cert
		csr_pem = crypto_util.make_csr(pkey_pem, [domain])
		csr_comp = jose.ComparableX509(crypto.load_certificate_request(crypto.FILETYPE_PEM, csr_pem))
		cert_res = self.client_acme.request_issuance(csr_comp, [authz])
		chain = self.client_acme.fetch_chain(cert_res)
		
		# Save cert
		cert_filepath = prefix + '-cert.pem'
		with open(cert_filepath, 'wb') as fh:
			fh.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert_res.body.wrapped))
		chain_filepath = prefix + '-fullchain.pem'
		with open(chain_filepath, 'wb') as fh:
			for item in chain:
				fh.write(crypto.dump_certificate(crypto.FILETYPE_PEM, item))
		
		return (pkey_filepath, cert_filepath, chain_filepath)

def contains_valid_chall(authz) -> bool:
	for challb in authz.body.challenges:
		if challb.status is messages.STATUS_VALID:
			return True
	return False

class ChallengeResponder:
	CHALLENGE_TYPE = None
	
	def select_challenge(self, authz):
		challenge_type = self.CHALLENGE_TYPE
		if challenge_type is None:
			raise NotImplementedError('ChallengeResponder.select_challenge')
		
		for challb in authz.body.challenges:
			if isinstance(challb.chall, challenge_type):
				return challb
		raise None
	
	def setup_challenge(self, chall, response, validation, client_acme):
		raise NotImplementedError('ChallengeResponder.setup_challenge')
	
	def teardown_challenge(self, chall):
		raise NotImplementedError('ChallengeResponder.teardown_challenge')

@contextmanager
def challenge_responder_context(challenge_responder, chall, response, validation, client_acme):
	try:
		challenge_responder.setup_challenge(chall, response, validation, client_acme)
		yield
	finally:
		challenge_responder.teardown_challenge(chall)
